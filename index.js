const express = require('express');

const PORT = 7777;

const app = express();

app.listen(PORT, () => console.log(`server started on port ${PORT}`));
